﻿using Automobile.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automobile.ViewModels
{
    public class AutoMotorsViewModel
    {
        public Auto Auto { get; set; }
        public List<Motor> Motors { get; set; }
        public int SelectedMotorId { get; set; }
    }
}