﻿function validateFormAuto(form) {
    var res = true;

    if (form.Auto_Manufacturer.value.length < 2) {
        $('#manufacturer1').text("jQuery kaze: Proizvodjac mora da ima u nazivu izmedju 2 i 50 karaktera.");
        res = false;
    }
    else {
        $('#manufacturer1').text("");
    }

    if (form.Auto_Model.value.length < 2) {
        $('#model1').text("jQuery kaze: Model mora da bude izmedju 2 i 50 karaktera.");
        res = false;
    }
    else {
        $('#model1').text("");
    }

    if (form.Auto_Year.value.length < 1 || form.Auto_Year.value < 1885 || form.Auto_Year.value > 2018) {
        $('#year1').text("jQuery kaze: Godina mora da bude izmedju 1885 i 2018.");
        res = false;
    }
    else {
        $('#year1').text("");

    }

    if (form.Auto_Cubic.value.length < 1 || form.Auto_Cubic.value < 500 || form.Auto_Cubic.value > 10000) {
        $('#cubic1').text("jQuery kaze: Kubikaza mora da bude broj od 500 do 10.000.");
        res = false;
    }
    else {
        $('#cubic1').text("");
    }

    if (form.Auto_Color.value.length < 2) {
        $('#color1').text("jQuery kaze: Boja mora da ima izmedju 2 i 50 karaktera.");
        res = false;
    }
    else {
        $('#color1').text("");
    }

    return res;
}

function validateFormMotor(form) {
    //var name2 = $(form).val();
    if (form.Name.value.length < 2) {
        $('#name1').text("jQuery kaze: Naziv mora da bude izmedju 2 i 50 karaktera.");
        //$("#Name1").attr("class", "field-validation-error text-danger");
        //$("#Name").addClass("input-validation-error")
        //$("#Name1").removeClass("field-validation-error")
        //$("#Name")
        //$(form).attr("data-val", "false");
        //$(form).attr("data-val-required", "Ohoho");
        //jQuery("#valodation_msg").css({ "color": "red" });
        //jQuery("#valodation_msg").html("Error");
        return false;
    }
    $('#name1').text();
    return true;
    //$("#someinput").rules("add", {
    //    required: true,
    //    messages: {
    //        required: "a value is required to save changes"
    //    }
    //});
}