﻿--- BIRAMO SA KOJOM BAZOM ZELIMO DA RADIMO
	use Automobile

--- UBACIVANJE PODATAKA
	insert into Motors (MotorName)
		values ('Motor1');
	insert into Motors (MotorName)
		values ('Motor2');
	insert into Motors (MotorName)
		values ('Motor3');
	insert into Motors (MotorName)
		values ('Motor4');

	insert into Autos (AutoManufacturer, AutoModel, AutoYear, AutoCubic, AutoColor, MotorId)
		values ('Audi', 'A3', 1996, 1795, 'Red', 1);
	insert into Autos (AutoManufacturer, AutoModel, AutoYear, AutoCubic, AutoColor, MotorId)
		values ('Audi', 'A4', 2001, 1795, 'Black', 1);
	insert into Autos (AutoManufacturer, AutoModel, AutoYear, AutoCubic, AutoColor, MotorId)
		values ('Audi', 'A5', 2007, 1895, 'White', 2);
	insert into Autos (AutoManufacturer, AutoModel, AutoYear, AutoCubic, AutoColor, MotorId)
		values ('Audi', 'A6', 2008, 1895, 'Silver', 2);
	insert into Autos (AutoManufacturer, AutoModel, AutoYear, AutoCubic, AutoColor, MotorId)
		values ('Audi', 'A7', 2010, 1995, 'Red', 3);
	insert into Autos (AutoManufacturer, AutoModel, AutoYear, AutoCubic, AutoColor, MotorId)
		values ('Audi', 'A8', 2011, 1995, 'Black', 3);
	insert into Autos (AutoManufacturer, AutoModel, AutoYear, AutoCubic, AutoColor, MotorId)
		values ('BMW', 'X5', 2013, 3495, 'Dark Grey', 4);
	insert into Autos (AutoManufacturer, AutoModel, AutoYear, AutoCubic, AutoColor, MotorId)
		values ('BMW', 'X7', 2016, 3495, 'Dark Grey', 4);