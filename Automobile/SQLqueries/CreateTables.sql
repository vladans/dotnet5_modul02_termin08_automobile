﻿--- BIRAMO SA KOJOM BAZOM ZELIMO DA RADIMO
	use Automobile

--- BRISANJE TABELE AKO POSTOJI
	drop table if exists Motors;
	drop table if exists Autos;

--- KREIRANJE TABELE
	CREATE TABLE Motors(
		MotorId INT PRIMARY KEY IDENTITY (1, 1),		/* gledati da naziv kolone uvek sadrzi prvo naziv tabele u imenu --> MotorId */
		MotorName NVARCHAR(50) NOT NULL UNIQUE
	)

	CREATE TABLE Autos(
		AutoId INT PRIMARY KEY IDENTITY (1, 1),		/* gledati da naziv kolone uvek sadrzi prvo naziv tabele u imenu --> AutoId */
		AutoManufacturer NVARCHAR(50),
		AutoModel NVARCHAR(50),
		AutoYear INT,
		AutoCubic INT,
		AutoColor NVARCHAR(50),
		MotorId INT,
		FOREIGN KEY (MotorId) REFERENCES Motors(MotorId),
	)