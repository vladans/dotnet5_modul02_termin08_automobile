﻿using Automobile.Interfaces;
using Automobile.Models;
using Automobile.Repository;
using Automobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automobile.Controllers
{
    public class AutosController : Controller
    {
        private IAutoRepository aRepository = new AutoRepository();
        private IMotorRepository mRepository = new MotorRepository();

        // GET: Autos
        public ActionResult Index()
        {
            var autos = aRepository.GetAll();
            foreach (var item in autos)
            {
                var res = mRepository.GetById(item.Motor.Id);
                item.Motor = res;
            }
            return View(autos);
        }

        // GET: Auto
        public ActionResult Create()
        {
            var motors = mRepository.GetAll();
            AutoMotorsViewModel amvm = new AutoMotorsViewModel();
            amvm.Motors = motors;
            return View(amvm);
        }

        // POST: Auto
        [HttpPost]
        public ActionResult Create(AutoMotorsViewModel amvm)
        {
            if (ModelState.IsValid)
            {
                amvm.Auto.Motor.Id = amvm.SelectedMotorId;
                var res = aRepository.Create(amvm.Auto);
                if (res)
                {
                    return RedirectToAction("Index");
                }
            }

            var motors = mRepository.GetAll();
            amvm.Motors = motors;

            return View(amvm);
        }

        // GET: Auto
        public ActionResult Edit(int id)
        {
            AutoMotorsViewModel amvm = new AutoMotorsViewModel();
            amvm.Auto = aRepository.GetById(id);
            amvm.Motors = mRepository.GetAll();
            amvm.SelectedMotorId = amvm.Auto.Motor.Id;
            return View(amvm);
        }

        // GET: Auto
        [HttpPost]
        public ActionResult Edit(AutoMotorsViewModel amvm)
        {
            if (ModelState.IsValid)
            {
                amvm.Auto.Motor.Id = amvm.SelectedMotorId;
                var res = aRepository.Update(amvm.Auto);
                if (res)
                {
                    return RedirectToAction("Index");
                }
            }
            var motors = mRepository.GetAll();
            amvm.Motors = motors;
            return View(amvm);
        }

        // GET: Auto
        public ActionResult Delete(int id)
        {
            var res = aRepository.Delete(id);
            if (res)
            {
                return RedirectToAction("Index");
            }
            return Content("Greska prilikom brisanja");
        }
    }
}