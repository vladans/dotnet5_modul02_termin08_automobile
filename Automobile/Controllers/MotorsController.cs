﻿using Automobile.Interfaces;
using Automobile.Models;
using Automobile.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automobile.Controllers
{
    public class MotorsController : Controller
    {
        private IMotorRepository mRepository = new MotorRepository();

        // GET: Motors
        public ActionResult Index()
        {
            var res = mRepository.GetAll();
            return View(res);
        }

        // GET: Motor
        public ActionResult Create()
        { 
            return View();
        }

        // POST: Motor
        [HttpPost]
        public ActionResult Create(Motor motor)
        {
            if (ModelState.IsValid)
            {
                var res = mRepository.Create(motor);
                return RedirectToAction("Index");
            }
            return View(motor);
        }

        // GET: Motor
        public ActionResult Edit(int id)
        {
            var res = mRepository.GetById(id);
            return View(res);
        }

        // GET: Motor
        [HttpPost]
        public ActionResult Edit(Motor motor)
        {
            if (ModelState.IsValid)
            {
                var res = mRepository.Update(motor);
                return RedirectToAction("Index");
            }
            return View(motor);
        }

        // GET: Motor
        public ActionResult Delete(int id)
        {
            var res = mRepository.Delete(id);
            return RedirectToAction("Index");
        }
    }
}