﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Automobile.Models
{
    public class Auto
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Ko je {0}?")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Minimum number of characters is 2, and maximum is 50")]
        [Display(Name = "Proizvodjac")]
        public string Manufacturer { get; set; }

        [Required(ErrorMessage = "Unesi naziv {0}a")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Minimum number of characters is 2, and maximum is 50")]
        [Display(Name = "Model")]
        public string Model { get; set; }

        [Required(ErrorMessage = "This is required field!")]
        [Range(1885, 2018, ErrorMessage = "{0} proizvodnje mora biti izmedju 1885 i 2018")]
        [Display(Name = "Godina")]
        public int Year { get; set; }

        [Required(ErrorMessage = "This is required field!")]
        [Range(500, 10000, ErrorMessage = "Automobil mora da ima izmedju 500 i 10000 {0}a")]
        [Display(Name = "Kubik")]
        public int Cubic { get; set; }

        [Required(ErrorMessage = "{0} je obavezno polje")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Minimum number of characters is 2, and maximum is 50")]
        [Display(Name = "Boja")]
        public string Color { get; set; }

        public Motor Motor { get; set; }

        public Auto()
        {
            this.Motor = new Motor();
        }

        public Auto(int id, string manufacturer, string model, int year, int cubic, string colour) : this()
        {
            this.Id = id;
            this.Manufacturer = manufacturer;
            this.Model = model;
            this.Year = year;
            this.Cubic = cubic;
            this.Color = colour;
        }
    }
}