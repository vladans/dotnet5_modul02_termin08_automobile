﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Automobile.Models
{
    public class Motor
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Unesi {0} care molim te :)")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Minimum number of characters is 2, and maximum is 50")]
        [Display(Name = "Naziv")]
        public string Name { get; set; }

        public List<Auto> Autos { get; set; }

        public Motor()
        {
            this.Autos = new List<Auto>();
        }

        public Motor(string name) : this()
        {
            this.Name = name;
        }

        public Motor(int id, string name) : this(name)
        {
            this.Id = id;
        }
    }
}