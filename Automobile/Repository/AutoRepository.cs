﻿using Automobile.Interfaces;
using Automobile.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Automobile.Repository
{
    public class AutoRepository : IAutoRepository
    {
        private SqlConnection conn;
        private void Connection()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["AutomobileConnection"].ConnectionString;
            conn = new SqlConnection(connectionString);
        }

        public bool Create(Auto auto)
        {
            try
            {
                string query = "INSERT INTO Autos (AutoManufacturer, AutoModel, AutoYear, AutoCubic, AutoColor, MotorId) VALUES (@AutoManufacturer, @AutoModel, @AutoYear, @AutoCubic, @AutoColor, @MotorId);";
                query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

                Connection();   // inicijaizuj novu konekciju

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@AutoManufacturer", auto.Manufacturer);
                    cmd.Parameters.AddWithValue("@AutoModel", auto.Model);
                    cmd.Parameters.AddWithValue("@AutoYear", auto.Year);
                    cmd.Parameters.AddWithValue("@AutoCubic", auto.Cubic);
                    cmd.Parameters.AddWithValue("@AutoColor", auto.Color);
                    cmd.Parameters.AddWithValue("@MotorId", auto.Motor.Id);

                    conn.Open();                            // otvori konekciju
                    var newId = cmd.ExecuteScalar();        // izvrsi upit nad bazom, vraca id novododatog zapisa
                    conn.Close();                           // zatvori konekciju

                    if (newId != null)
                    {
                        return true;    // upis uspesan, generisan novi id
                    }
                }
                return false;   // upis bezuspesan
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom upisa novog auta. " + ex.StackTrace);  // ispis u output-prozorcicu
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            bool res = false;
            try
            {
                string query = "DELETE FROM Autos WHERE AutoId = @AutoId;";
                Connection();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@AutoId", id);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    res = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom brisanja automobila. " + ex.StackTrace);  // ispis u output prozoru
                throw ex;
            }
            return res;
        }

        public List<Auto> GetAll()
        {
            List<Auto> autos = new List<Auto>();
            try
            {
                string query = @"SELECT * FROM Autos a, Motors m
                                WHERE a.MotorId = m.MotorId ORDER BY AutoId";

                //string query = "SELECT * FROM Autos ORDER BY AutoId";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dataAdapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dataAdapter.Fill(ds, "Autos"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["Autos"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    conn.Close();                  // zatvori konekciju
                }
                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int id = int.Parse(dataRow["AutoId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string manufacturer = dataRow["AutoManufacturer"].ToString();
                    string model = dataRow["AutoModel"].ToString();
                    int year = int.Parse(dataRow["AutoYear"].ToString());
                    int cubic = int.Parse(dataRow["AutoCubic"].ToString());
                    string color = dataRow["AutoColor"].ToString();
                    int motorId = int.Parse(dataRow["MotorId"].ToString());
                    //string motorName = dataRow["MotorName"].ToString();

                    Auto auto = new Auto(id, manufacturer, model, year, cubic, color);
                    auto.Motor.Id = motorId;
                    //auto.Motor.Name = motorName;
                    autos.Add(auto);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izlistavanja automobila. {ex.StackTrace}");
                throw ex;
            }
            return autos;
        }

        public Auto GetById(int id)
        {
            try
            {
                //string query = @"SELECT * FROM Autos WHERE AutoId = @AutoId;";
                //string query = "SELECT * FROM Autos a, Motors m WHERE a.AutoId = @AutoId AND m.MotorId = a.MotorId;";
                string query = @"
                    SELECT *
                    FROM Autos a
                    INNER JOIN Motors m
                    ON a.MotorId = m.MotorId
                    WHERE AutoId = @AutoId;";

                Connection();
                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@AutoId", id);

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    //pribavimo bookstorove
                    dadapter.Fill(ds, "Autos");
                    dt = ds.Tables["Autos"];
                    conn.Close();

                    if (dt.Rows.Count == 1)
                    {
                        int autoId = int.Parse(dt.Rows[0]["AutoId"].ToString());
                        string manufacturer = dt.Rows[0]["AutoManufacturer"].ToString();
                        string model = dt.Rows[0]["AutoModel"].ToString();
                        int year = int.Parse(dt.Rows[0]["AutoYear"].ToString());
                        int cubic = int.Parse(dt.Rows[0]["AutoCubic"].ToString());
                        string color = dt.Rows[0]["AutoColor"].ToString();
                        int motorId = int.Parse(dt.Rows[0]["MotorId"].ToString());
                        string motorName = dt.Rows[0]["MotorName"].ToString();

                        Auto auto = new Auto(autoId, manufacturer, model, year, cubic, color);
                        auto.Motor.Id = motorId;
                        auto.Motor.Name = motorName;

                        return auto;
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom dobavljanja automobila po Id-ju. " + ex.StackTrace);  // ispis u outputprozoru
                throw ex;
            }
        }

        public bool Update(Auto auto)
        {
            bool res = false;
            try
            {
                string query = @"
                    UPDATE Autos
                    SET AutoManufacturer = @AutoManufacturer, AutoModel = @AutoModel, AutoYear = @AutoYear, AutoCubic = @AutoCubic, AutoColor = @AutoColor, MotorId = @MotorId
                    WHERE AutoId = @AutoId;";

                Connection();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@AutoId", auto.Id);
                    cmd.Parameters.AddWithValue("@AutoManufacturer", auto.Manufacturer);
                    cmd.Parameters.AddWithValue("@AutoModel", auto.Model);
                    cmd.Parameters.AddWithValue("@AutoYear", auto.Year);
                    cmd.Parameters.AddWithValue("@AutoCubic", auto.Cubic);
                    cmd.Parameters.AddWithValue("@AutoColor", auto.Color);
                    cmd.Parameters.AddWithValue("@MotorId", auto.Motor.Id);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    res = true;
                    return res;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom azuriranja automobila. " + ex.StackTrace);  // ispis u output prozoru
                //return res;
                throw ex;
            }
        }
    }
}