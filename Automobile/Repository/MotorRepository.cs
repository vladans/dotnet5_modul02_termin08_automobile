﻿using Automobile.Interfaces;
using Automobile.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Automobile.Repository
{
    public class MotorRepository : IMotorRepository
    {
        private SqlConnection conn;
        private void Connection()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["AutomobileConnection"].ConnectionString;
            conn = new SqlConnection(connectionString);
        }

        public bool Create(Motor motor)
        {
            try
            {
                string query = "INSERT INTO Motors (MotorName) VALUES (@MotorName);";
                query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

                Connection();   // inicijaizuj novu konekciju

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@MotorName", motor.Name);

                    conn.Open();                            // otvori konekciju
                    var newId = cmd.ExecuteScalar();        // izvrsi upit nad bazom, vraca id novododatog zapisa
                    conn.Close();                           // zatvori konekciju

                    if (newId != null)
                    {
                        return true;    // upis uspesan, generisan novi id
                    }
                }
                return false;   // upis bezuspesan
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom upisa novog motora. " + ex.StackTrace);  // ispis u output-prozorcicu
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            bool res = false;
            try
            {
                string query = "DELETE FROM Motors WHERE MotorId = @MotorId;";
                Connection();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@MotorId", id);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    res = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom brisanja motora. " + ex.StackTrace);  // ispis u output prozoru
                throw ex;
            }
            return res;
        }

        public List<Motor> GetAll()
        {
            List<Motor> motors = new List<Motor>();
            try
            {
                string query = "SELECT * FROM Motors ORDER BY MotorId";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dataAdapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dataAdapter.Fill(ds, "Motors"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["Motors"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    conn.Close();                  // zatvori konekciju
                }
                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int id = int.Parse(dataRow["MotorId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string name = dataRow["MotorName"].ToString();

                    motors.Add(new Motor(id, name));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izlistavanja motora. {ex.StackTrace}");
                throw ex;
            }
            return motors;
        }

        public Motor GetById(int id)
        {
            try
            {
                //string query = "SELECT * FROM Motors m INNER JOIN Autos a ON m.MotorId = a.MotorId WHERE MotorId = @MotorId;";
                string query = "SELECT * FROM Motors WHERE MotorId = @MotorId;";
                Connection();
                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@MotorId", id);

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    //pribavimo bookstorove
                    dadapter.Fill(ds, "Motors");
                    dt = ds.Tables["Motors"];
                    conn.Close();

                    if (dt.Rows.Count == 1)
                    {
                        int motorId = int.Parse(dt.Rows[0]["MotorId"].ToString());
                        string name = dt.Rows[0]["MotorName"].ToString();

                        return new Motor(motorId, name);
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom dobavljanja motora po Id-ju. " + ex.StackTrace);  // ispis u outputprozoru
                throw ex;
            }
        }

        public bool Update(Motor motor)
        {
            bool res = false;
            try
            {
                string query = "UPDATE Motors SET MotorName = @MotorName WHERE MotorId = @MotorId;";
                Connection();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@MotorId", motor.Id);
                    cmd.Parameters.AddWithValue("@MotorName", motor.Name);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    res = true;
                    return res;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom azuriranja motora. " + ex.StackTrace);  // ispis u output prozoru
                //return res;
                throw ex;
            }
        }
    }
}