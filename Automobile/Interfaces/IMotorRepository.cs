﻿using Automobile.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automobile.Interfaces
{
    interface IMotorRepository
    {
        List<Motor> GetAll();
        Motor GetById(int id);
        bool Create(Motor motor);
        bool Update(Motor motor);
        bool Delete(int id);
    }
}
