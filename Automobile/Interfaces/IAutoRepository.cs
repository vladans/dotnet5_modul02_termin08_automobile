﻿using Automobile.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automobile.Interfaces
{
    interface IAutoRepository
    {
        List<Auto> GetAll();
        Auto GetById(int id);
        bool Create(Auto auto);
        bool Update(Auto auto);
        bool Delete(int id);
    }
}
